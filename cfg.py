from mmcv import Config
from mmdet.apis import set_random_seed

from dataset import XMLCustomDataset

cfg = Config.fromfile("mmdetection/configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py")
print(f"Default Config:\n{cfg.pretty_text}")

# Modify dataset type and path.
cfg.dataset_type = "XMLCustomDataset"
cfg.data_root = "data/NEU-DET/"

cfg.data.test.type = "XMLCustomDataset"
cfg.data.test.data_root = "data/NEU-DET/"
cfg.data.test.ann_file = "valid.txt"
cfg.data.test.img_prefix = ""

cfg.data.train.type = "XMLCustomDataset"
cfg.data.train.data_root = "data/NEU-DET/"
cfg.data.train.ann_file = "train.txt"
cfg.data.train.img_prefix = ""

cfg.data.val.type = "XMLCustomDataset"
cfg.data.val.data_root = "data/NEU-DET/"
cfg.data.val.ann_file = "valid.txt"
cfg.data.val.img_prefix = ""

# Batch size (samples per GPU).
cfg.data.samples_per_gpu = 2

# Modify number of classes as per the model head.
cfg.model.roi_head.bbox_head.num_classes = 6
# Comment/Uncomment this to training from scratch/fine-tune according to the
# model checkpoint path.
cfg.load_from = "checkpoints/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth"

# The output directory for training. As per the model name.
cfg.work_dir = "outputs/faster_rcnn_r50_fpn_1x_coco_fine_tune"

# The original learning rate (LR) is set for 8-GPU training.
# We divide it by 8 since we only use one GPU.
cfg.optimizer.lr = 0.02 / 8
cfg.lr_config.warmup = None
cfg.log_config.interval = 5

# Evaluation Metric.
cfg.evaluation.metric = "mAP"
# Evaluation times.
cfg.evaluation.interval = 5
# Checkpoint storage interval.
cfg.checkpoint_config.interval = 5

# Set random seed for reproducible results.
cfg.seed = 0
set_random_seed(0, deterministic=False)
cfg.gpu_ids = range(1)
cfg.device = "cuda"
cfg.runner.max_epochs = 10

# We can also use tensorboard to log the training process
cfg.log_config.hooks = [
    dict(type="TextLoggerHook"),
    dict(type="TensorboardLoggerHook"),
    dict(
        type="NeptuneLoggerHook",
        init_kwargs=dict(project="user/project", api_token="netpune_token"),
    ),
]

# We can initialize the logger for training and have a look
# at the final config used for training
print(f"Config:\n{cfg.pretty_text}")
