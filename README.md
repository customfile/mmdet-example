# README



## Steps to Train

***Ensure that you have installed MMCV and MMDetection before proceeding with the following steps.***

* Clone the [MMDetection](https://github.com/open-mmlab/mmdetection.git) repository.

```
git clone https://github.com/open-mmlab/mmdetection.git
```

* Download the Pascal VOC 2007 data using the following commands in the terminal.
  * Or use these download links.
    * [Download VOC 2007 trainval from here](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar).
    * [Download VOC 2007 test from here](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar).


```
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar

wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
```

* Extract the Pascal VOC 2007 data.

```
tar xf VOCtrainval_06-Nov-2007.tar -C data

tar xf VOCtest_06-Nov-2007.tar -C data
```

* Download the weights.

```
python download_weights.py
```

* Train the model.

```
python train.py
```

* Inference on videos.

```
python inference_video.py --input inference_data/video_1.mp4
```

​	With threshold.

```
python inference_video.py --input inference_data/video_1.mp4 --threshold 0.25
```



## Image / Video Credits and Attributions

* `input/`
  * `video_1.mp4`: Video by Tim  Samuel: https://www.pexels.com/video/cabs-passing-through-the-streets-of-new-york-city-5834623/.
    * https://www.pexels.com/video/cabs-passing-through-the-streets-of-new-york-city-5834623/
  * `video_2.mp4`: Video by ROMAN ODINTSOV: https://www.pexels.com/video/drone-shot-of-people-riding-horses-6577809/.
    * https://www.pexels.com/video/drone-shot-of-people-riding-horses-6577809/.
